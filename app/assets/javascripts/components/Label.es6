import React from "react"
import PropTypes from "prop-types"

class Label extends Component {
  render () {
    return (
      <React.Fragment>
        Label: {this.props.label}
      </React.Fragment>
    );
  }
}

Label.propTypes = {
  label: PropTypes.string
};
