class AppointmentForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      title: this.props.title,
      appt_time: this.props.appt_time
    }
  }

  handleInput(e) {
    const name = e.target.name
    obj = {}
    obj[name] = e.target.value
    this
      .props
      .onUserInput(obj)
  }

  handleSubmit(e) {
    e.preventDefault()

    this
      .props
      .onFormSubmit()
  }

  setApptTime(e) {
    var name = "appt_time"
    var obj = {}
    if (obj[name] = e.toDate()) {
      this.props.onUserInput(obj)
    }
  }

  render() {
    var inputProps = {
      name: 'appt_time'
    }

    return (
      <div>
        <h2>Make a new appointment</h2>
        <form onSubmit={(event) => this.handleSubmit(event)}>
          <input
            name="title"
            placeholder="Appointment Title"
            value={this.props.input_title}
            onChange={(event) => this.handleInput(event)}/>

          <Datetime inputProps={inputProps} value={this.props.appt_time} open={true} input={false} onChange={(event) => this.setApptTime(event)}/>

          <input type="submit" value="Make Appointment" className='submit-button'/>
        </form>
      </div>
    )
  }
}
